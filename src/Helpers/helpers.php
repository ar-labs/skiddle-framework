<?php

use amineabri\Config\Config;
use amineabri\View\View;
use App\SKIDDLE\Services\SkiddlePanigation;

if (! function_exists('View')) {

    /**
     * Get the evaluated View contents for the given view.
     *
     * @param null $view
     * @param null $data
     * @param null $items
     * @return View|void
     * @throws SmartyException
     */
    function View($view = null, $data = null, $items = null)
    {
        $factory = new View;

        if (func_num_args() === 0) {
            return $factory;
        }

        return $factory->make($view, $data, $items);
    }
}

if (! function_exists('dd')) {

    function dd($data){
        if(is_array($data) || is_object($data)){
            echo "<pre>";
            print_r($data);
        }else{
            var_dump($data);
        }
        exit;
    }
}


if (! function_exists('pagination')) {

    function pagination($totalItems, $itemsPerPage, $currentPage, $urlPattern){
        $paginator = new SkiddlePanigation($totalItems, $itemsPerPage, $currentPage, $urlPattern);
        return $paginator;
    }
}


if (! function_exists('send_json')) {
    function send_json( $response ) {
        @header( 'Content-Type: application/json; charset=UTF-8' );
        echo json_encode( $response );
        exit;
    }
}



if (! function_exists('config')) {
    function config( $name ) {
        $config = new Config();
        return $config->get($name);
    }
}

if (! function_exists('route')) {
    /**
     * Generate the URL to a named route.
     *
     * @param  array|string  $name

     * @return array
     */
    function route($name){
//        $router = new Router;
//        return $router->getRoute($name);
    }
}
