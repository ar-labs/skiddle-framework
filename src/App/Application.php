<?php
namespace amineabri\App;
use amineabri\Access\ErrorHandler;
use amineabri\Config\Config;

class Application {
    static $path;

    public function __construct($path){
        self::$path	  =	$path;
    }

    public static function run($routes = true,$whoops = true){
        $config = new Config();
        ini_set("log_errors", 1);
        ini_set("error_log", self::$path.'/'.$config->get('app.loggin.log_path'));
        if($whoops){
            ErrorHandler::ini();
        }

        if($routes){
            include_once self::$path."/routes/web.php";
        }
    }

}
