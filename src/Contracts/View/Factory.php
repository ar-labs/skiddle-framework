<?php
namespace amineabri\Contracts\View;
use amineabri\Config\Smarty\SmartyTemplate;

abstract class Factory extends SmartyTemplate {

    /**
     * Get the evaluated view contents for the given view.
     *
     * @param $view
     * @param null $data
     * @param null $items
     * @throws \SmartyException
     */
    public function make($view, $data = null, $items = null){
        $root = $_SERVER['DOCUMENT_ROOT'];
        if(!is_null($data)){
            if(!is_null($items)){
                $this->appends($data ,$items);
            }else{
                foreach ($data as $key => $value){
                    $this->assign($key, $value);
                }
            }
        }
        $view_path = $root.'/../resources/views/';
        return $this->display($view_path.$view.".tpl.php");
    }

    /**
     * Assign an array of data to a view
     *
     * @param $data
     * @param $item
     */
    public  function appends($data, $item){
        if(is_array($data) && count($data) > 0){
            foreach($data as $value){
                $this->append($item, $value);
            }
        }
    }
}
