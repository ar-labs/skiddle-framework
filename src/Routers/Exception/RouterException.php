<?php
namespace amineabri\Routers\Exception;
use amineabri\Config\Config;
use Exception;

class RouterException
{
    /**
     * @var bool $debug Debug mode
     */
    public static $debug = false;

    /**
     * Create Exception Class.
     *
     * @param $message
     *
     * @return string
     * @throws Exception
     */
    public function __construct($message){
        self::$debug = $this->getDefaultParams();
        if (self::$debug) {
            throw new Exception($message, 1);
        } else {
            header("{$_SERVER["SERVER_PROTOCOL"]} 404 Not Found");
            return View('404');
        }
    }

    /**
     * @return bool|null
     */
    protected function getDefaultParams(){
        $config = new Config();
        $params = $config->get('app.loggin.debug');
        return $params;
    }
}
