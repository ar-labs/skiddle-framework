<?php
namespace amineabri\Config;
use Symfony\Component\Yaml\Yaml;

class Config extends Yaml{
    static $config;

    /**
     * @param $name
     * @return bool|null
     */
    public function get($name) {
        if($name){
            $root         = $_SERVER['DOCUMENT_ROOT'];
            self::$config = $this->parseFile($root.'/../config/app.yml');
            $args         = explode(".", $name);
            return $this->get_value(self::$config, $args);
        }
        return null;
    }

    /**
     * @param $data_arr
     * @param $data_arr_call
     * @return bool
     */
    private function get_value($data_arr, $data_arr_call) {
        foreach ($data_arr_call as $index) {
            if (isset($data_arr[$index])) {
                $data_arr = $data_arr[$index];
            } else {
                return false;
            }
        }
        return $data_arr;
    }
}
