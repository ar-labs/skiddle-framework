<?php
namespace amineabri\Config\Smarty;
use amineabri\Config\Config;
use Smarty;

class SmartyTemplate extends Smarty {
    function __construct() {
        parent::__construct();
        $root   = $_SERVER['DOCUMENT_ROOT'];
        $config = new Config();
        $this->compile_check 	= $config->get('app.template.compile_check');
        $this->force_compile 	= $config->get('app.template.force_compile');
        $this->debugging 	 	= $config->get('app.template.debugging');
        $this->caching 			= $config->get('app.template.caching');
        $this->use_sub_dirs		= $config->get('app.template.use_sub_dirs');
        $this->cache_lifetime 	= $config->get('app.template.cache_lifetime');
        $this->left_delimiter 	= $config->get('app.template.left_delimiter');
        $this->right_delimiter 	= $config->get('app.template.right_delimiter');
        $this->smarty_debug_id 	= $config->get('app.template.smarty_debug_id');
        $this->clearAllCache($config->get('app.template.cache_exp_time'));
        $this->setTemplateDir($root.'/../'.$config->get('app.template.template_dir'));
        $this->setCompileDir($root.'/../'.$config->get('app.template.compile_dir'));
        $this->setCacheDir($root.'/../'.$config->get('app.template.cache_dir'));
        $this->setPluginsDir([
            $root.'/../'.$config->get('app.template.plugin_dir'),
        ]);
    }
}
