<?php
namespace amineabri\Access;
use Whoops\Handler\PlainTextHandler;
use amineabri\Config\Config;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

class ErrorHandler {

    public static function ini() {
        $config = new Config();
        if($config->get('app.loggin.debug')) {
            $whoops    = new Run;
            $errorPage = new PrettyPageHandler();
            if(!is_null($config->get('app.loggin.page_title'))){
                $errorPage->setPageTitle($config->get('app.loggin.page_title')); // Set the page's title
            }
            $errorPage->setEditor($config->get('app.loggin.editor'));
            $whoops->pushHandler($errorPage);
            $whoops->register();
        }
    }

}
